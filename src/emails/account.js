const sgMail = require('@sendgrid/mail');

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const sendWelcomeEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'asadzeynal9@gmail.com',
        subject: 'Thanks for joining in!',
        text: `Welcome to the app, ${name}. Let me know how you get along with the app.`
    });
};

const sendCancelationEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'asadzeynal9@gmail.com',
        subject: 'Tell us why you leave',
        text: `Dear, ${name}, we are sad you leave. Tell us the reason by replying to this email.`
    })
};

module.exports = {
    sendWelcomeEmail,
    sendCancelationEmail,
}